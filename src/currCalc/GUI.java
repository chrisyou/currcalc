/**
 * 
 */
package currCalc;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Locale;

import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

/**
 * @author gatsby
 * Filename: GUI.java
 * Comment: Displays the GUI and calls for Calc.java
 */
public class GUI extends JFrame implements ActionListener{
	private Container cp = getContentPane();
	public JTextField txtForeign, txtRate, txtOwn;
	private JButton btnStart, btnClear, btnClose, btnSwitch;
	private JPanel pnNorth, pnCenter, pnSouth;
	private JLabel lblOwn, lblForeign;
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		new GUI();

	}
	public GUI(){
		setTitle("currCalc");
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		cp.setLayout(new BorderLayout());
		createComponents();
	}
	/**
	 * 
	 */
	private void createComponents() {
		// TODO Auto-generated method stub
		txtRate = new JTextField(" 1:650 ");		
		txtRate.setHorizontalAlignment(JTextField.CENTER);
		txtForeign = new JTextField(10);
		txtForeign.setHorizontalAlignment(JTextField.CENTER);
		txtForeign.setEnabled(false);
		txtOwn = new JTextField(10);	
		txtOwn.setHorizontalAlignment(JTextField.CENTER);
		btnStart = new JButton();
		btnClear = new JButton();
		btnClose = new JButton();		
		ImageIcon icon = new ImageIcon("res/icon.png");
		Image img = icon.getImage();
		Image newImg = img.getScaledInstance(16, 16, java.awt.Image.SCALE_SMOOTH);
		ImageIcon newIcon = new ImageIcon(newImg);
		btnSwitch = new JButton(newIcon);
		lblOwn = new JLabel();
		lblForeign = new JLabel();		
		localize();
		createLayout();
	}
	/**
	 * 
	 */
	private void localize() {
		// TODO Auto-generated method stub
		Locale sysLocale = Locale.getDefault();
		String strLocale = sysLocale.getLanguage();
		if(strLocale.equalsIgnoreCase("de")){
			btnStart.setText("Berechnen");
			btnClear.setText("Werte l�schen");
			txtRate.setToolTipText("Gib den Wechselkurs an. Format: 'Ausgangsw�hrung:Schlie�liche W�hrung'");
			btnClose.setText("Schlie�en");
			btnClear.setToolTipText("L�scht alle Werte bis auf den Kurs.");
			btnStart.setToolTipText("Startet die Berechnung.");
			lblForeign.setText("- Zu");
			lblOwn.setText("Von -");
				

		}else{
			btnStart.setText("Calculate");
			btnClear.setText("Clear values");
			txtRate.setToolTipText("Enter the current exchange rate here. Format: 'your currency:foreign currency'");
			btnClose.setText("Close");
			btnClear.setToolTipText("Clears all values but the exchange rate.");
			btnStart.setToolTipText("Launches the calculation.");
			lblForeign.setText("- To");
			lblOwn.setText("From -");
		}

	}
	/**
	 * 
	 */
	private void createLayout() {
		// TODO Auto-generated method stub#
		pnNorth = new JPanel(new FlowLayout());
		pnCenter = new JPanel(new FlowLayout());
		pnSouth = new JPanel(new FlowLayout());
		pnNorth.add(txtRate);
		pnCenter.add(lblOwn);
		pnCenter.add(txtOwn);
		pnCenter.add(btnSwitch);
		pnCenter.add(txtForeign);
		pnCenter.add(lblForeign);
		pnSouth.add(btnClear);
		pnSouth.add(btnStart);
		pnSouth.add(btnClose);
		cp.add(pnCenter,BorderLayout.CENTER);
		cp.add(pnSouth,BorderLayout.SOUTH);
		cp.add(pnNorth,BorderLayout.NORTH);
		pack();
		setLocationRelativeTo(null);
		setVisible(true);
		setResizable(false);
	}
	/* (non-Javadoc)
	 * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
	 */
	@Override
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub

	}

}

